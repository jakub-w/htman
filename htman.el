;;; htman.el --- A browser for documentation in the HTML format -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Jakub Wojciech <jakub-w@riseup.net>
;; URL: https://codeberg.org/jakub-w/htman
;; Keywords: docs, help
;; Package: htman
;; Verison: 0.0.1
;; Package-Requires: ((emacs "26.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; For some time now I've been annoyed by the recent trend of people
;; publishing their project documentation only on the web in the HTML format.
;; This forces us, users, to browse these docs using an external web browser
;; without convenient facilities of our editor, like is the case with
;; `info-mode'.
;; This project strives to achieve a generic way of pulling documentation
;; from the internet straight into Emacs to be viewed offline.
;; It automatically creates an index for the docs, and a node hierarchy,
;; enabling commands such as `htman-lookup-symbol' (analogous to
;; `info-lookup-symbol') or `htman-goto-node' (analogous to `Info-goto-node').

;;; Code:

(require 'shr)
(require 'cl-lib)

(defcustom htman-goto-new-buffer-method #'switch-to-buffer-other-window
  "Behavior after creating a new htman buffer.

Needs to be a function taking a buffer or a buffer name argument."
  :type 'function
  :options (list #'switch-to-buffer
		 #'switch-to-buffer-other-window
		 #'switch-to-buffer-other-frame
		 #'switch-to-buffer-other-tab)
  :group 'htman)

(defcustom htman-cache-directory
  (concat user-emacs-directory "htman/")
  "A directory where the index cache is stored.

Setting this variable through customize will move the previously set
directory to the new path, keeping the contents intact."
  :type 'directory
  :set (lambda (optname value)
	 (when (and (boundp 'htman-cache-directory)
		    htman-cache-directory
		    (file-directory-p htman-cache-directory))
	   (copy-directory htman-cache-directory value
			   'keep-time 'parents 'copy-contents)
	   (delete-directory htman-cache-directory))
	 (set-default 'htman-cache-directory value))
  :group 'htman)

(defcustom htman-download-rate-limit "500k"
  "Limit of internet bandwidth usage in bytes when calling
`htman-download-documentation'. With a `k' and `M' suffix, treat the number
as kilobytes and megabytes respectively."
  :type 'string
  :group 'htman)

;; TODO: Add htman--render-title that will show the title with
;;       header-line-format instead of the document itself.
;;       Also parse the title as utf8 (probably?).
(defvar htman-shr-rendering-functions
  '((img . htman--render-img)))

(defvar htman-download-url-preprocessors nil)

(defvar-local htman-project-name nil
  "A name of the project for which the documentation is loaded currently.")
(defvar-local htman-project-version nil
  "A version of the project for which the documentation is loaded currently.")

(defvar-local htman-top-dir nil)
(defvar-local htman-current-path nil)
;; workaround for shr using temp buffers internally
(defvar htman--current-path-shr nil)

(defvar-local htman-index nil)

(defvar htman-dom-filter-function #'identity
  "A function taking a DOM as parsed from `libxml-parse-html-region' as an
argument and returning a modified version of it to be rendered.

The intended use is removing menus to show only the page content.
It saves the user from having to scroll through them every time a page is
shown. Especially since there's a built-in menu system.

This variable shouldn't be set manually.")

(defvar htman-html-filter-function nil
  "A function used to alter the html contents of a file before parsing to a
sexp DOM.
This function will run in a file buffer of the html file being processed.")

(defvar-local htman-forward-history nil)
(defvar-local htman-backward-history nil)

(defvar htman-indexer-parse-id-function #'identity
  "A function used by the generic indexer to parse the id of a reference
in an html file to a form used as a key in the node index.
Takes a string argument.

For example, if we expect <section id=\"my@strange__tag\"> in a file, we can
set this variable to a function that will process the id attribute to
\"my-pretty-tag\" that will act as a key in the node index.")

(defvar htman-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "RET") #'htman-follow-link)
    (define-key map (kbd "<tab>") #'htman-move-to-next-link)
    (define-key map (kbd "<backtab>") #'htman-move-to-previous-link)
    (define-key map (kbd "r") #'htman-history-forward)
    (define-key map (kbd "l") #'htman-history-backward)
    (define-key map (kbd "w") #'htman-copy-current-page-name)
    map))

(define-derived-mode htman-mode special-mode "htman"
  "Mode for browsing arbitrary documentation in HTML format."
  (unless nil nil))

(defmacro define-htman-type (name &rest keyword-args)
  "Define a type NAME for documentation.

The specified functions will be used to download and analyze the
documentation. They will be only called as a setup on every download
and the information on how to parse the documentation will be saved in the
documentation's top-level directory file called \"htman-descriptor\".

All functions specified by KEYWORD-ARGS take a DIR argument - the directory
of the project's documentation, unless stated otherwise.
All keyword args specified below are required.

KEYWORD-ARGS are:
:predicate
   Function returning nil if DIR doesn't contain this type of documentation
   and non-nil otherwise.

:get-name-fun
   Function returning the name of the project the documentation is for.

:get-version-fun
   Analogous to :get-name-fun, but returning the version.

:get-index-fun
   Function returning the index for the documentation. The index is a
   hash table with keys being the page names and the values
   `htman--uri' objects. It should contain all of the nodes that
   should be accessible through `htman-index'. The function's result
   will be cached.

:get-doc-tree-fun
   Function returning a hierarchy of nodes with human-friendly
   chapter names.
   An element in a tree is (display-name uri child ...) or
   (display-name child ...) if the element itself is not navigable and will
   be unnumbered in the table of contents.
   uri is htman--uri object.
   Children follow the same specification.
   The whole tree is ('top element ...).
   Example:
   (top (\"GENERAL\"
         (\"Chapter 1: How to make a documentation\"
          (make-htman--uri :path \"chapters/how-to-make-a-doc.html\")
         (\"Defining documentation hierarchy\"
          (make-htman--uri :path \"chapters/how-to-make-a-doc.html\"
                             :anchor \"hierarchy-definition\")
         ...)
         (\"Chapter 2: Using the documentation\" ...)
         ...)

:filter-page-fun
   Function taking a DOM argument (as returned from
   `libxml-parse-html-region') and returning a modified DOM with everything
   that is not relevant for rendering a page removed.
   For example, all the navigation, search and other menus should be removed.
   Only the page content should stay.
"
  ;; `(list #',(plist-get keyword-args :predicate)
  ;; 	 #',(plist-get keyword-args :project-name)
  ;; 	 #',(plist-get keyword-args :project-version)
  ;; 	 #',(plist-get keyword-args :project-index)
  ;; 	 #',(plist-get keyword-args :doc-tree))
  `(list #',(plist-get keyword-args :predicate)
	 #',(plist-get keyword-args :get-name-fun)
	 #',(plist-get keyword-args :get-version-fun)
	 #',(plist-get keyword-args :get-index-fun)
	 #',(plist-get keyword-args :get-copyright-fun)
	 #',(plist-get keyword-args :get-doc-tree-fun)
	 #',(plist-get keyword-args :filter-page-fun))
  )

(defun htman--assert-mode ()
  (unless (eq major-mode 'htman-mode)
    (user-error "[htman] Not in htman-mode")))

(cl-defstruct htman--uri path anchor)

(defun htman--parse-path-internal (path)
  (if (string-match "\\(.*\\)#\\(.+\\)$" path)
      (make-htman--uri
       :path (match-string-no-properties 1 path)
       :anchor (match-string-no-properties 2 path))
    (make-htman--uri :path path)))

;; FIXME: This should create paths relative to the documentation's top dir.
;; FIXME: If path is just an anchor, refer to the current file.
(defun htman--parse-path (path)
  (if (file-name-absolute-p path)
      (htman--parse-path-internal path)
    (htman--parse-path-internal
     (concat (file-name-directory htman-current-path) path))))

(defun htman--render-page ()
  (let* ((shr-external-rendering-functions htman-shr-rendering-functions)
	 ;; Replace shr bindings for buttons
	 (shr-map htman-mode-map)
	 (inhibit-read-only t)
	 (htman--current-path-shr htman-current-path))
    (erase-buffer)
    (shr-insert-document
     (funcall htman-dom-filter-function
	      (with-temp-buffer
		(insert-file-contents htman--current-path-shr)
		(when htman-html-filter-function
		  (funcall htman-html-filter-function))
		(libxml-parse-html-region (point-min) (point-max)))))
    (goto-char (point-min))))

(defun htman-copy-current-page-name ()
  "Copy currently visited page's name to the kill ring."
  (interactive)
  (htman--assert-mode)
  ;; FIXME: This should copy some unique uri that could be used to go to
  ;;        the current page from outide the mode with (htman uri).
  ;;        For now this is a placeholder.
  ;;   Consider making links to the closest anchor by default, and to the
  ;;   page with a prefix argument.
  (let ((name htman-current-path))
    (kill-new name)
    (message "%s" name)))

(defun htman-move-to-next-link ()
  "Move point to the next link/button."
  (interactive)
  (let ((inhibit-message t))
    (text-property-search-forward 'button t)))

(defun htman-move-to-previous-link ()
  "Move point to the previous link/button."
  (interactive)
  (let ((inhibit-message t))
    (text-property-search-backward 'button)))

(defun htman-history-forward ()
  "Go forward in history of visited pages."
  (interactive)
  (htman--assert-mode)
  (if (null htman-forward-history)
      (message "This is the most recently visited page")
    (let ((place (pop htman-forward-history)))
      (push (cons htman-current-path (point))
	    htman-backward-history)
      (setq-local htman-current-path (car place))
      (htman--render-page)
      (goto-char (cdr place)))))

(defun htman-history-backward ()
  "Go backward in history of visited pages."
  (interactive)
  (htman--assert-mode)
  (if (null htman-backward-history)
      (message "This is the first visited page")
    (let ((place (pop htman-backward-history)))
      (push (cons htman-current-path (point))
	    htman-forward-history)
      (setq-local htman-current-path (car place))
      (htman--render-page)
      (goto-char (cdr place)))))

;; FIXME: This should work on relative paths stored in htman--uri.
;;        (For now they're absolute).
(defun htman--goto-page (uri)
  "URI must be an htman--uri object."
  (htman--assert-mode)
  (when htman-current-path
    (push (cons htman-current-path (point))
	  htman-backward-history))
  (setq-local htman-forward-history nil
	      ;; FIXME: This should use a relative path.
	      htman-current-path (htman--uri-path uri))
  (htman--render-page)
  (when-let ((anchor (htman--uri-anchor uri)))
    (let ((start (point)))
      (goto-char (point-min))
      (unless (text-property-search-forward 'shr-target-id anchor #'member)
	(goto-char start)))))

(defun htman-follow-link (&optional mouse-event)
  "Follow link at point."
  (interactive (list last-nonmenu-event))
  (mouse-set-point mouse-event)
  (let* ((url (get-text-property (point) 'shr-url))
	 (h-uri (ignore-errors (htman--parse-path url))))
    (cond
     ((not url)
      (message "No link under point"))
     ((string-match "^mailto:" url)
      (browse-url-mail url))
     ((file-exists-p (htman--uri-path h-uri))
      (htman--goto-page h-uri))
     (t
      (user-error "[htman] Couldn't resolve link: %s" url)))))

(defun htman--render-img (dom &optional url)
  (let ((url (or url (dom-attr dom 'src)))
	(alt (or (dom-attr dom 'alt) "*"))
	(width (shr-string-number (dom-attr dom 'width)))
        (height (shr-string-number (dom-attr dom 'height))))
    (cond ((null url))
	  ((or (member (dom-attr dom 'height) '("0" "1"))
	       (member (dom-attr dom 'width) '("0" "1")))
	   ;; Ignore zero-sized or single-pixel images.
	   )
	  ((string-match "\\`data:" url)
	   (let ((image (shr-image-from-data (substring url (match-end 0)))))
	     (if image
		 (funcall shr-put-image-function image alt
                          (list :width width :height height))
	       (insert alt))))
	  ((string-match "\\`[a-z]+:" url)
	   ;; Only local files will be rendered.
	   (insert alt))
	  (t
	   (let ((filename
		  (concat (file-name-directory htman--current-path-shr) url)))
	     (if (file-exists-p filename)
		 (insert-image (create-image filename nil nil
					     :width width
					     :height height))
	       (insert alt)))))))

;; HACK:
(defun htman--shr-descend-advice (descend dom)
  "Force shr-descend to put shr-target-id text property on all tags with
an id attribute."
  (if (eq major-mode 'htman-mode)
      (let ((shr-target-id (dom-attr dom 'id)))
	(funcall descend dom))
    (funcall descend dom)))

;; This is needed only in versions prior to 28 because the new shr-descend
;; does what we want by default.
(when (< emacs-major-version 28)
  (advice-add 'shr-descend :around #'htman--shr-descend-advice))


;;; Generic indexer
;; It will recursively walk over all html files in a directory and collect
;; every id attribute.

;; If we save a path as a list of directory/file names, we can share these
;; strings between multiple objects, saving some memory.
(defun htman--make-path (&rest parts)
  parts)
(defun htman--path-p (obj)
  (and (consp obj) (seq-every-p #'stringp obj)))
(defun htman--path= (p1 p2) (equal p1 p2))
(defun htman--path-append (path &rest parts)
  (append path parts))
(defun htman--path-to-string (path)
  (string-join path "/"))
(defun htman--path-file-exists-p (path)
  (file-exists-p (htman--path-to-string path)))

;; (defun htman--indexer-parse-file (current-path filename index)
;;   (with-temp-buffer
;;     (insert-file-contents filename)
;;     (while (re-search-forward "<[^>]+id=\"\\([^\"]+\\)\"" nil 'noerror)
;;       (let ((id (match-string-no-properties 1)))
;; 	(push (cons (funcall htman-indexer-parse-id-function id)
;; 		    (htman--path-append current-path
;; 					  (concat filename "#" id)))
;; 	      index))))
;;   index)

;; (defun htman--run-generic-indexer-1 (current-path dir index)
;;   (let* ((all (directory-files dir))
;; 	 (default-directory dir)
;; 	 (files (seq-filter
;; 		 (lambda (filename) (string-match-p "\\.html$" filename))
;; 		 all))
;; 	 (dirs (seq-filter
;; 		(lambda (filename)
;; 		  (and (file-directory-p filename)
;; 		       (not (string-match-p "\\(?:^\\|/\\)\\.\\.?$"
;; 					    filename))))
;; 		all)))
;;     (seq-reduce
;;      (lambda (idx directory)
;;        (htman--run-generic-indexer-1
;; 	(htman--path-append current-path directory)
;; 	(concat dir "/" directory)
;; 	idx))
;;      dirs
;;      (seq-reduce (lambda (idx filename)
;; 		   (htman--indexer-parse-file current-path filename idx))
;; 		 files
;; 		 index))))

;; (defun htman--run-generic-indexer (dir)
;;   (message "[htman] Started the generic indexer")
;;   (htman--run-generic-indexer-1 (htman--make-path) dir '())
;;   (message "[htman] Indexing done"))

(defun htman--run-generic-indexer (dir)
  "Recursively scan directory DIR for any html files and generate an index
of all id anchors.

The index will contain paths relative to DIR, so it should point to root
directory of the documentation.

An anchor is an id attribute of any html tag. E.g. <section id=\"my-tag\"/>.

It runs `htman-indexer-parse-id-function' on every id string before adding
it to the index.

Note that this won't create seperate entries for anchors with the same id.
Only the last one will be included in the index."
  (message "[htman] Started the generic indexer. Please wait...")
  (with-temp-buffer
    ;; current-path, current-dir and current-file are used as a hack to not
    ;; store duplicated strings in the index. We want the most data sharing
    ;; possible since indexes are typically long.
    (let ((start-time (current-time))
	  current-path
	  (current-dir "")
	  (current-file "")
	  (index (make-hash-table :size (count-lines (point-min)
						     (point-max))
				  :test #'equal)))
      ;; TODO: Run rg, ag or ack if found, fallback to grep.
      (call-process "grep" nil t nil
		    "--recursive"
		    "--extended-regexp"
		    "<[^>]+id=\"[^\"]+\""
		    dir)
      (goto-char (point-min))

      (while (re-search-forward "^\\([^:]+\\)[^<]+<[^>]+id=\"\\([^\"]+\\)\""
				nil 'noerror)
	(let* ((id (funcall htman-indexer-parse-id-function
			    (match-string-no-properties 2)))
	       (path (file-relative-name (match-string-no-properties 1) dir))
	       (path-directory (let ((path-dir (file-name-directory path)))
				 (if (string= path-dir current-dir)
				     current-dir
				   (setq current-dir path-dir))))
	       (path-filename (let ((path-file (file-name-nondirectory path)))
				(if (string= path-file current-file)
				    current-file
				  (setq current-file path-file)))))
	  (puthash id
		   (make-htman--uri
		    :path (let ((p (htman--make-path path-directory
						       path-filename)))
			    (if (htman--path= p current-path)
				current-path
			      (setq current-path p)))
		    :anchor id)
		   index)))
      (message "[htman] Indexing done. Took %.06f s."
	       (float-time (time-since start-time)))
      index)))

;;; End of generic indexer


;;;###autoload
(defun htman-open-directory (&optional dir)
  "Open DIR in `htman-mode'."
  (interactive
   (list (read-directory-name "Directory: " htman-cache-directory)))
  ;; TODO: include doc name in buffer name
  ;; FIXME: If the documentation for DIR is already open, don't generate
  ;;        new buffers.
  (let* ((buffer (generate-new-buffer "*htman*")))
    (with-current-buffer buffer
      (htman-mode)
      (setq-local htman-top-dir dir)
      (htman--goto-page (htman--parse-path (concat dir "index.html")))
      (funcall htman-goto-new-buffer-method buffer))))

;;;###autoload
(defun htman-lookup-symbol (symbol)
  "Search the current documentation's index for SYM and open the selected
node in `htman-mode'."
  (interactive (list (completing-read "Node: " htman-index)))
  (if-let ((path (htman--index-ref symbol)))
      (htman--goto-page path)
    (user-error "[htman] No such node: %s" symbol)))

(defun htman--index-ref (symbol)
  (gethash symbol htman-index))

(defun htman--url-domain (url)
  (string-match "\\(?:https?://\\)?\\(?:www\\)?\\([^/]+\\)" url)
  (match-string-no-properties 1 url))


(defun htman-download-documentation (url)
  (interactive "sURL: ")
  ;; This would use all the matching preprocessors:
  ;; (mapc (lambda (elt)
  ;; 	    (when (string-match-p (car elt) url)
  ;; 	      (setq url (funcall (cdr elt) url))))
  ;; 	  htman-download-url-preprocessors)

  ;; Use just the first preprocessor:
  (and-let* ((found (cl-find-if
		     (lambda (elt) (string-match-p (car elt) url))
		     htman-download-url-preprocessors)))
    (setq url (funcall (cdr found) url)))
  (message "Downloading documentation from %s..." url)

  (let ((buffer (get-buffer-create "*htman-download-log*")))
    (make-process
     :name "htman-download-documentation"
     :buffer buffer
     :command
     `("wget"
       "--no-parent"
       "--no-clobber"
       ,(concat "--directory-prefix="
		(substitute-in-file-name
		 (expand-file-name htman-cache-directory)))
       ,(concat "--limit-rate=" htman-download-rate-limit)
       "--recursive"
       "--level=inf"
       "--adjust-extension"
       "--convert-links"
       "--page-requisites"
       ,(concat "--domains=" (htman--url-domain url))
       "--timestamping"
       ,url))
    (pop-to-buffer buffer))
  ;; TODO: Download to the directory named after the github repo.
  ;;       --no-host-directories, --cut-dirs
  ;; TODO: Save the current date in a cache file to be used as a doc version.
  )




(provide 'htman)
;;; htman.el ends here

;; TODO: Read the index file to get the filter spec that will be applied
;;       to every pages dom before rendering.
;;       Used for removing menu from pages.
;;
;; TODO: When parsing html build the current page's reference list.
;;       In info-mode 'f' lets you follow any reference on the current page.

;; TODO: Consider saving (window-start) and (point) in history to recreate
;;       the state of the visited pages more closely. (First align the window
;;       to have the same window-start as before, then move point to the old
;;       position).

;; TODO: Entrypoint for this mode should be a buffer with a documentation name
;;       and a table of contents.

;; TODO: Add a way of resolving http links?

;; TODO: Documentation about generic indexer.

;; FIXME: RET on an image throws an error.
;;        It should open the image in a new buffer perhaps?

;; FIXME: Maybe resize images too big to fit the buffer?
;;        After pressing RET on them a new buffer should appear showing
;;        the image in full size.

;; FIXME: htman--uri should store a path relative to the top dir to save
;;        space.

;; An example of a wget for getting a website:
;; wget				\
;;   --no-clobber		\
;;   --limit-rate=500k		\
;;   --recursive		\
;;   --level=inf		\
;;   --adjust-extension		\
;;   --convert-links		\
;;   --page-requisites		\
;;   --domains=docs.panda3d.org	\
;;   --timestamping		\
;;   URL
