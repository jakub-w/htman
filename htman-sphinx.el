;;; -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Jakub Wojciech <jakub-w@riseup.net>
;; Keywords: docs, help
;; Package: htman

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;: Code:

(require 'htman)

(defun htman-sphinx--objfile (&optional dir)
  (if dir
      (concat dir "objects.inv")
    (htman--assert-mode)
    (concat htman-top-dir "objects.inv")))

(defun htman--is-sphinx-directory-p (dir)
  (and (file-directory-p dir)
       (let ((objfile (concat dir "objects.inv")))
	 (and (file-exists-p objfile)
	      (with-temp-buffer
		(insert-file-contents-literally objfile nil 0 100)
		(looking-at-p "# Sphinx inventory version 2"))))))



(defun htman-sphinx--parse-index-file-line ()
  "Parse the objects.inv line as per specification in
`https://sphobjinv.readthedocs.io/en/v2.1/syntax.html'."
  (and (re-search-forward
	(rx (group-n 1 (+ not-newline)) whitespace
	    (group-n 2 (+ (not (any whitespace ?:)))) ?:
	    (group-n 3 (+ (not (any whitespace ?:)))) whitespace
	    (group-n 4 (? ?-) (+ digit)) whitespace
	    (? (group-n 5 (+ (not whitespace))) whitespace)
	    (group-n 6 (+ not-newline)) line-end)
	(point-at-eol)
	'noerror)

       (let* ((name (match-string-no-properties 1))
	      (domain (match-string-no-properties 2))
	      (role (match-string-no-properties 3))
	      (priority (string-to-number (match-string-no-properties 4)))
	      (uri (match-string-no-properties 5))
	      (dispname (match-string-no-properties 6)))
	 ;; NOTE:  If dispname is identical to name it's set to "-".
	 (when (string= dispname "-") (setq dispname name))
	 ;; NOTE: Uri can have an anchor equal to "$", which means that the
	 ;;       real anchor is the value of name but was omitted to save
	 ;;       space.
	 (setq uri
	       (if (string-match "\\(.*\\)#\\([^/]+\\)$" uri)
		   (let ((anchor (match-string-no-properties 2 uri)))
		     (make-htman--uri
		      :path (match-string-no-properties 1 uri)
		      :anchor (if (string= anchor "$") name anchor)))
		 (make-htman--uri :path uri)))
	 (list name
	       :domain domain :role role :priority priority
	       :uri uri :dispname dispname))))

(defun htman-sphinx--index-from-objfile (dir)
  (let ((objfile (htman-sphinx--objfile dir)))
    (and (file-exists-p objfile)
	 (with-temp-buffer
	   (set-buffer-multibyte nil)
	   (insert-file-contents-literally objfile)
	   (re-search-forward "^[^#]")
	   (goto-char (point-at-bol))
	   (zlib-decompress-region (point) (point-max))
	   (let* ((num-entries (count-lines (point) (point-max)))
		  (index (make-hash-table :test #'equal :size num-entries))
		  entry)
	     (while (setq entry (htman-sphinx--parse-index-file-line))
	       (puthash (car entry) (plist-get (cdr entry) :uri) index)
	       ;; (puthash (car entry) (cdr entry) index)
	       (forward-char))
	     index)))))

;; FIXME: This is heavy on allocation. First it builds the dom, then it
;;        filters it twice, and only then it builds the hash table.
;;        Indexes have sometimes tens of thousands of entries.
(defun htman-sphinx--index-from-genindex (dir)
  (let ((genindex (concat dir "genindex.html")))
    (and
     (file-exists-p genindex)
     (with-temp-buffer
       (insert-file-contents genindex)
       (search-forward "<h1 id=\"index\">")
       (let* ((links (dom-by-tag
		      (append '(dummy nil)
			      (dom-by-class
			       (libxml-parse-html-region (match-beginning 0)
							 (point-max))
			       "indextable"))
		      'a))
	      (index (make-hash-table :size (length links) :test #'equal)))
	 (mapc
	  (lambda (a)
	    ;; TODO: Parse to htman--uri.
	    (puthash (dom-text a) (dom-attr a 'href) index))
	  links)
	 index)))))


(defun htman-sphinx--generate-index (dir)
  (or (htman-sphinx--index-from-objfile dir)
      (htman-sphinx--index-from-genindex dir)))

(defun htman-sphinx--dom-texts-between (str1 str2)
  "Searches the current html buffer, starting at current position.
Moves the point."
  (search-forward str1 nil 'noerror)
  (and-let* ((start (match-end 0))
	     (end (search-forward str2 nil 'noerror))
	     (result (string-trim
		      (dom-texts (libxml-parse-html-region start end)))))
    (and (not (string-empty-p result))
	 result)))

(defun htman-sphinx--get-project-from-genindex (dir)
  (let ((genindex (concat dir "genindex.html")))
    (when (file-exists-p genindex)
      (with-temp-buffer
	(insert-file-contents genindex)
	(let (result)
	  (while (not (setq result (htman-sphinx--dom-texts-between
				    "<a href=\"index.html\">" "</a>"))))
	  result)))))

(defun htman-sphinx--get-version-from-genindex (dir)
  (let ((genindex (concat dir "genindex.html")))
    (when (file-exists-p genindex)
      (with-temp-buffer
	(insert-file-contents genindex)
	(htman-sphinx--dom-texts-between
	 "<div class=\"version\">" "</div>")))))

(defun htman-sphinx--get-project-and-version-from-objfile (dir)
  (let ((objfile (htman-sphinx--objfile dir))
	name version)
    (and (file-exists-p objfile)
	 (progn
	   (with-temp-buffer
	     (insert-file-contents-literally objfile nil 0 500)
	     (re-search-forward "Project: \\(.*\\)")
	     (setq name (match-string-no-properties 1))
	     (re-search-forward "Version: \\(.*\\)")
	     (setq version (match-string-no-properties 1)))
	   (cons name version)))))

(defun htman-sphinx--project-name (dir)
  (if-let ((p+v (htman-sphinx--get-project-and-version-from-objfile dir)))
      (car p+v)
    (htman-sphinx--get-project-from-genindex dir)))

(defun htman-sphinx--project-version (dir)
  (if-let ((p+v (htman-sphinx--get-project-and-version-from-objfile dir)))
      (cdr p+v)
    (htman-sphinx--get-project-from-genindex dir)))

(defun htman-sphinx-generic--generate-tree (dir)
  (cl-labels
      ((descend-toc
	(node)
	;; In Sphinx multiple toctrees can be specified and each can have a
	;; caption. In the html output the caption is not a part of the
	;; hierarchy, it is a node with a `p' tag and the toctree for it
	;; follows on the same level, not as its child.
	;; That's why we have top-lst and lst, top-lst is there in case we
	;; encounter a `p' node that signifies the end of the current
	;; toctree and the beginning of a new one, so we move what's in lst
	;; to top-lst and create a new lst.
	(let (top-lst lst)
	  (dolist (child (dom-children node))
	    (unless (stringp child)
	      (case (car child)
		((p)
		 (when lst (push (nreverse lst) top-lst))
		 (setq lst
		       (list (dom-text (dom-by-class child "caption-text")))))
		((ul)
		 (setq lst (nconc (descend-toc child) lst)))
		((li)
		 (push (descend-toc child) lst))
		((a)
		 (push (dom-text child) lst)
		 (push (htman--parse-path-internal (dom-attr child 'href))
		       lst))
		(t (error "Tag unhandled: %s" (car child))))))
	  (if (null top-lst)
	      (nreverse lst)
	    (progn (push (nreverse lst) top-lst)
		   (nreverse top-lst))))))
    (with-temp-buffer
      (insert-file-contents (concat dir "index.html"))
      (let* ((dom (libxml-parse-html-region (point-min) (point-max)))
	     toc)
	;; Search for "wy-menu" class and use that. If not found, search for
	;; "toctree-" and go 2 parents up.
	(when (null (setq toc (dom-by-class dom "wy-menu")))
	  (setq toc
		(dom-parent
		 dom
		 (dom-parent
		  dom
		  (car (dom-by-class dom "toctree-"))))))
	(cons 'top (descend-toc toc))))))

(defun htman-sphinx-generic--get-copyright (dir)
  (with-temp-buffer
    (insert-file-contents (concat dir "index.html"))
    (search-forward "role=\"contentinfo\"")
    (search-backward "<div")
    (let ((start (point)))
      (search-forward "</div>")
      (string-trim
       (dom-texts
	;; NOTE: Parse so that the html entities get resolved.
	(libxml-parse-html-region start (point)))))))

(defun htman-sphinx-generic--filter-dom (dom)
  (car (dom-elements dom 'role "main")))

(define-htman-type sphinx-generic
  :predicate htman--is-sphinx-directory-p
  :get-name-fun htman-sphinx--project-name
  :get-version-fun htman-sphinx--project-version
  :get-index-fun htman-sphinx--generate-index
  :get-copyright-fun htman-sphinx-generic--get-copyright
  :get-doc-tree-fun htman-sphinx-generic--generate-tree
  :filter-page-fun htman-sphinx-generic--filter-dom)

(provide 'htman-sphinx)

;; FIXME: Provide a way of generating the index without using objects.inv.
;;        The same goes for the project name and version.
;;        Also predicate function.
;;        This file wouldn't be available if we used wget to download the
;;        documentation, and this is the plan.

;; TODO:
;; the names of nodes from genindex.html are a parsed, human-readable version
;; of 'objects.inv's dispname with more data from domain and role.
;; If I want to use both methods I need to find a way of unifying the format.

;; NOTE: It may be possible that the genindex.html doesn't have any references
;;       throughoug

;; NOTE: It seems the name for the node doesn't always is the text in the
;;       'a' node. In panda3d's index there is a whole section of links with
;;       'module' as a text, but prepended with the real name that is not a
;;       link.
;; NOTE: The index in genindex.html is kinda strange. If multiple objects
;;       have a method of the same name they will be listed in the index as
;;       a group. The first occurence will have the name in the node's text,
;;       but the rest that will follow won't, they will be contained in the
;;       node after the first one, grouped in a seperate <ul> list.
;; NOTE: In the genindex.html some of method entries are written like
;;       "Class::fun() (C++ function)", and some as "fun() (Class method)".
;;       I guess it's the difference between C++ index and python index.
;;       But it seems that these "python" classes are really c++ classes,
;;       so why the difference?
;; Seems like the genindex.html isn't as fullproof as I'd like.
